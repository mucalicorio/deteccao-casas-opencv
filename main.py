# encoding: utf-8

'''''''''''''''''''''''''''''''''''''''''''''''''''''
#####################################################
# __author__ = "Samuel Licorio Leiva"               #
# __credits__ = ["Leandro Leonardo Passarelli",     #
#                "Lucas de Brito Silva",            #
#                "Paulo Sérgio de Oliveira Júnior", #
#                "Ricardo Heiji"]                   #
# __license__ = "GPL"                               #
# __version__ = "0.0.1"                             #
# __email__ = "samuel.licorio@gmail.com"            #
# __status__ = "Learning"                           #
#####################################################
'''''''''''''''''''''''''''''''''''''''''''''''''''''

import cv2
import numpy as np
# from matplotlib import pyplot as plt


def embacar():
    print('Def embacar()')

    kernel = np.ones((2,2),np.uint8)
    dilate = cv2.dilate(imagem,kernel,iterations=1)
    gaussianImg = cv2.GaussianBlur(dilate,(5,5), 1)
    gaussianAnt = cv2.GaussianBlur(imagem,(5,5), 1)

    # filtro1 = cv2.blur(imagem,(1,9))
    # filtro2 = cv2.medianBlur(imagem,11)
    # filtro3 = cv2.GaussianBlur(imagem,(11,11),cv2.BORDER_DEFAULT)
    # filtro4 = cv2.bilateralFilter(imagem,9,75,75)

    # cv2.imshow('Imagem Embaçada 1', filtro1)
    # cv2.imshow('Imagem Embaçada 2', filtro2)
    # cv2.imshow('Imagem Embaçada 3', filtro3)
    # cv2.imshow('Imagem Embaçada 4', filtro4)
    # cv2.imshow('Embaçando', np.hstack((filtro3, filtro4)))
    cv2.imshow('Dilatado', dilate)
    cv2.imshow('Gaussiano Novo', gaussianImg)
    cv2.imshow('Gaussiano Anti', gaussianAnt)
    print('Imagem Embaçada')


def main():
    print('Def main()')
    cv2.imshow('Imagem Redimensionada', imagem)

    print('_____________________________________')
    
    embacar()

    while True:
        fechar = cv2.waitKey(30)
        
        if (fechar == 27):
            cv2.destroyAllWindows()
            print('_____________________________________')
            print('Janelas Fechadas')
            print('_____________________________________')
            print('Finalizando')
            break



if __name__ == '__main__':
    import sys
    print('_____________________________________')
    print("if __name__ == '__main__'")
    try:
        param = sys.argv[1]
        print('_____________________________________')
        print('Parâmetro: ', param)
    except Exception as e:
        param = ('assets/imgs/image.png')
        print('_____________________________________')
        print(param)

    imagemOriginal = cv2.imread(param)
    imagem = cv2.resize(imagemOriginal, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_CUBIC)
    print('Imagem Redimensionada:', imagem.shape)
    print('_____________________________________')
    main()

# embace = cv2.blur(imagem,(5,5))

# plt.subplot(121),plt.imshow(imagem),plt.title('Imagem')
# plt.xticks([]),plt.yticks([])
# plt.subplot(122),plt.imshow(embace),plt.title('Embaçado')
# plt.xticks([]),plt.yticks([])
# plt.show()
